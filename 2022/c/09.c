#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define SET_INIT_CAP 256

struct pos {
	int32_t x;
	int32_t y;
};

struct set_item {
	struct pos pos;
	struct set_item *next;
};

struct set {
	size_t len;
	size_t cap;
	struct set_item *buckets[SET_INIT_CAP];
};

struct rope {
	size_t len;
	struct pos *knots;
};

void
set_init(struct set *s)
{
	s->len = 0;
	s->cap = SET_INIT_CAP;
	for (size_t i = 0; i < s->cap; ++i)
		s->buckets[i] = NULL;
}

void
set_deinit(struct set *s)
{
	struct set_item *p = NULL;
	struct set_item *q = NULL;
	for (size_t i = 0; i < s->cap; ++i) {
		for (p = s->buckets[i]; p != NULL; p = q) {
			q = p->next;
			free(p);
		}
	}
}

size_t
hash_pos(struct pos p)
{
	size_t pack = p.x;
	pack <<= 32;
	pack |= p.y;
	return pack;
}

void
set_add(struct set *s, struct pos pos)
{
	const size_t hash = hash_pos(pos);
	const size_t i = hash % s->cap;
	struct set_item **p;
	for (p = &s->buckets[i]; *p != NULL; p = &(*p)->next) {
		if ((*p)->pos.x == pos.x && (*p)->pos.y == pos.y)
			return;
	}
	*p = malloc(sizeof(**p));
	(*p)->pos = pos;
	(*p)->next = NULL;
	s->len += 1;
}

bool
set_has(const struct set *s, struct pos pos)
{
	const size_t hash = hash_pos(pos);
	const size_t i = hash % s->cap;
	for (struct set_item *p = s->buckets[i]; p != NULL; p = p->next) {
		if (p->pos.x == pos.x && p->pos.y == pos.y)
			return true;
	}
	return false;
}

void
rope_init(struct rope *rope, size_t len)
{
	rope->len = len;
	rope->knots = malloc(rope->len * sizeof(*rope->knots));
	for (size_t i = 0; i < rope->len; ++i) {
		rope->knots[i].x = 0;
		rope->knots[i].y = 0;
	}
}

void
rope_deinit(struct rope *rope)
{
	free(rope->knots);
}

int
dir_from_int(int n)
{
	if (n > 1)
		n -= 1;
	else if (n < -1)
		n += 1;
	return n;
}

void
update_knot(struct rope *rope, size_t n)
{
	struct pos *tail = &rope->knots[n];
	const struct pos head = rope->knots[n - 1];
	
	const int dx = tail->x - head.x;
	const int dy = tail->y - head.y;
	if (dx*dx > 1 || dy*dy > 1) {
		tail->x -= dir_from_int(dx);
		tail->y -= dir_from_int(dy);
	}

	if (n + 1 < rope->len)
		update_knot(rope, n + 1);
}

void
rope_move(struct rope *rope, struct pos dir, struct set *visited)
{
	rope->knots[0].x += dir.x;
	rope->knots[0].y += dir.y;
	update_knot(rope, 1);
	set_add(visited, rope->knots[rope->len - 1]);
}

struct pos
dir_from_char(char c)
{
	struct pos d = {0};
	if (c == 'R')
		d.x = 1;
	else if (c == 'L')
		d.x = -1;
	else if (c == 'U')
		d.y = 1;
	else if (c == 'D')
		d.y = -1;
	return d;
}

int
main(void)
{
	struct rope r1, r2;
	rope_init(&r1, 2);
	rope_init(&r2, 10);

	struct set vis1, vis2;
	set_init(&vis1);
	set_init(&vis2);

	FILE *f = stdin;
	for (int c = fgetc(f); c != EOF; c = fgetc(f)) {
		const struct pos d = dir_from_char(c);
		fgetc(f);

		int n = 0;
		for (c = fgetc(f); isdigit(c); c = fgetc(f))
			n = n * 10 + (c - '0');

		while (n--) {
			rope_move(&r1, d, &vis1);
			rope_move(&r2, d, &vis2);
		}
	}

	const size_t ans1 = vis1.len;
	const size_t ans2 = vis2.len;
	
	printf("#1: %zu\n", ans1);
	printf("#2: %zu\n", ans2);

	rope_deinit(&r1);
	rope_deinit(&r2);
	set_deinit(&vis1);
	set_deinit(&vis2);

	return 0;
}
