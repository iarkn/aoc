#include <stdio.h>
#include <string.h>

#if !defined(STACKS) && !defined(CRATES)
#error "STACKS and CRATES must be defined"
#endif

struct stack {
	size_t len;
	int data[CRATES];
};

int
stack_pushf(struct stack *s, int c)
{
	if (s->len >= CRATES) {
		return 1;
	}
	memmove(&s->data[1], &s->data[0], s->len * sizeof(*s->data));
	s->data[0] = c;
	s->len += 1;
	return 0;
}

int
stack_pushb(struct stack *s, int c)
{
	if (s->len >= CRATES) {
		return 1;
	}
	s->data[s->len] = c;
	s->len += 1;
	return 0;
}

void
stack_moveb(struct stack *s, struct stack *dst, int n)
{
	memmove(&dst->data[dst->len], &s->data[s->len - n],
			n * sizeof(*s->data));
	s->len -= n;
	dst->len += n;
}

int
stack_pop(struct stack *s)
{
	if (s->len <= 0) {
		return -1;
	}
	s->len -= 1;
	return s->data[s->len];
}

int
parsenum(FILE *f)
{
	int c, res = 0;
	for (c = fgetc(f); '0' <= c && c <= '9'; c = fgetc(f)) {
		res = res * 10 + c - '0';
	}
	ungetc(c, f);
	return res;
}

void
fnext(FILE *f, int n)
{
	while (n--) {
		fgetc(f);
	}
}

int
main(void)
{
	struct stack st1[STACKS] = {0};
	struct stack st2[STACKS] = {0};
	size_t ns = 0;
	int done = 0;
	while (!done) {
		int c = getchar();
		int e = 0;
		switch (c) {
		case '[':
			c = getchar();
			e = stack_pushf(&st1[ns], c);
			if (e != 0) {
				fprintf(stderr, "st1: pushf failed (%d)\n", e);
				return 1;
			}
			e = stack_pushf(&st2[ns], c);
			if (e != 0) {
				fprintf(stderr, "st2: pushf failed (%d)\n", e);
				return 1;
			}
			c = getchar();
			if (c != ']') {
				fprintf(stderr, "expected ']', got '%c' (%d)\n",
						c, c);
				return 1;
			}
			break;
		case ' ':
			ns += 1;
			c = getchar();
			if (c == ' ') {
				// Skip empty crate
				getchar();
				getchar();
			} else {
				ungetc(c, stdin);
			}
			break;
		case '\n':
			c = getchar();
			if (c == ' ') {
				// Skip the number line and go to the next phase
				while (getchar() != '\n') {}
				getchar();
				done = 1;
			} else {
				ungetc(c, stdin);
				ns = 0;
			}
			break;
		default:
			fprintf(stderr, "unknown token '%c' (%d)\n", c, c);
			return 1;
		}
	}

	int move = 0;
	size_t from = 0;
	size_t to = 0;
	for (int c = getchar(); !feof(stdin); c = getchar()) {
		int d = 0;
		int e = 0;
		switch (c) {
		case 'm':
			fnext(stdin, 4);
			move = parsenum(stdin);
			break;
		case 'f':
			fnext(stdin, 4);
			from = parsenum(stdin) - 1;
			break;
		case 't':
			fnext(stdin, 2);
			to = parsenum(stdin) - 1;
			break;
		case ' ':
			break;
		case '\n':
			stack_moveb(&st2[from], &st2[to], move);
			while (move--) {
				d = stack_pop(&st1[from]);
				if (d == -1) {
					fputs("pop failed", stderr);
					return 1;
				}
				e = stack_pushb(&st1[to], d);
				if (e != 0) {
					fprintf(stderr, "pushr failed (%d)\n", e);
					return 1;
				}
			}
			break;
		default:
			fprintf(stderr, "unknown token '%c' (%d)\n", c, c);
			return 1;
		}
	}

	char ans1[STACKS] = {0};
	char ans2[STACKS] = {0};
	for (size_t i = 0; i < STACKS; ++i) {
		size_t nb1 = st1[i].len - 1;
		ans1[i] = st1[i].data[nb1];
		size_t nb2 = st2[i].len - 1;
		ans2[i] = st2[i].data[nb2];
	}

	printf("#1: %s\n", ans1);
	printf("#2: %s\n", ans2);

	return 0;
}
