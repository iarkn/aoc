#include <stdio.h>

int parsenum(FILE *f)
{
    int res = 0, c;
    for (c = fgetc(f); '0' <= c && c <= '9'; c = fgetc(f)) {
        res = res * 10 + c - '0';
    }
    return res;
}

void try_setcal(int cal, int hc[3])
{
    if (cal > hc[0]) {
        hc[2] = hc[1];
        hc[1] = hc[0];
        hc[0] = cal;
    } else if (cal > hc[1]) {
        hc[2] = hc[1];
        hc[1] = cal;
    } else if (cal > hc[2]) {
        hc[2] = cal;
    }
}

int main(void)
{
    int cal = 0;
    int hc[3] = {0};
    while (!feof(stdin)) {
        int d = parsenum(stdin);
        cal += d;

        int c = getchar();
        if (c == '\n') {
            try_setcal(cal, hc);
            cal = 0;
        } else {
            ungetc(c, stdin);
        }
    }

    try_setcal(cal, hc);
    printf("#1: %d\n", hc[0]);
    printf("#1: %d\n", hc[0] + hc[1] + hc[2]);

    return 0;
}
