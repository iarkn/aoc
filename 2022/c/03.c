#include <stdio.h>
#include <string.h>

int strchrd(const char *src, int c, int delim)
{
    for (size_t i = 0; src[i] != delim; ++i) {
        if (src[i] == c) return 1;
    }
    return 0;
}

int sack_type(const char *a, const char *b)
{
    for (size_t i = 0; a[i] != '\n'; ++i) {
        if (strchrd(b, a[i], '\n')) return a[i];
    }
    return -1;
}

int group_badge(const char g[3][64])
{
    int grb_map[128] = {0};
    for (size_t i = 0; g[0][i] != '\n'; ++i) {
        char c = g[0][i];
        if (grb_map[c]) continue;
        grb_map[c] = 1;
        if (strchrd(g[1], c, '\n') && strchrd(g[2], c, '\n')) {
            return c;
        }
    }
    return -1;
}

int ctop(char c)
{
    if ('a' <= c && c <= 'z') {
        return c - 'a' + 1;
    } else if ('A' <= c && c <= 'Z') {
        return c - 'A' + 27;
    }
    return -1;
}

int main(void)
{
    int types = 0;
    int badges = 0;
    char group[3][64] = {0};
    char *buf = NULL;
    ssize_t nread;
    size_t nb;
    for (size_t i = 0; (nread = getline(&buf, &nb, stdin)) != -1; ++i) {
        size_t len = nread / 2;
        types += ctop(sack_type(buf, &buf[len]));
        strncpy(group[i], buf, nread);
        if (i == 2) {
            badges += ctop(group_badge(group));
            i = -1;
        }
    }

    free(buf);

    if (!feof(stdin) && nread == -1) {
        perror("getline");
        return 1;
    }

    printf("#1: %d\n", types);
    printf("#2: %d\n", badges);

    return 0;
}
