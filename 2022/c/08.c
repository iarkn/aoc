#include <stdio.h>

#ifndef LEN
#define LEN 99
#endif
#define SIZE (LEN * LEN)

const struct {
	int x;
	int y;
} pos[] = {
	{1, 0}, {-1, 0},
	{0, 1}, {0, -1},
};
char trees[SIZE];

int
is_edge(size_t x, size_t y)
{
	return x == 0 || y == 0 || x == LEN - 1 || y == LEN - 1;
}

int
is_visible(size_t x, size_t y)
{
	const size_t n = LEN * y + x;
	for (size_t i = 0; i < 4; ++i) {
		size_t j = 1;
		for (;;) {
			const size_t dx = x + pos[i].x * j;
			const size_t dy = y + pos[i].y * j;
			const size_t dn = LEN * dy + dx;
			if (trees[dn] >= trees[n])
				break;
			if (is_edge(dx, dy))
				return 1;
			j += 1;
		}
	}
	return 0;
}

int
visible_trees(void)
{
	int count = 0;
	for (size_t i = 0; i < SIZE; ++i) {
		const size_t x = i % LEN;
		const size_t y = i / LEN;
		if (is_edge(x, y) || is_visible(x, y))
			count += 1;
	}
	return count;
}

int
scenic_score(size_t x, size_t y)
{
	int score = 1;
	const size_t n = LEN * y + x;
	for (size_t i = 0; i < 4; ++i) {
		size_t j = 1;
		for (;;) {
			const size_t dx = x + pos[i].x * j;
			const size_t dy = y + pos[i].y * j;
			const size_t dn = LEN * dy + dx;
			if (trees[dn] >= trees[n] || is_edge(dx, dy))
				break;
			j += 1;
		}
		score *= j;
	}
	return score;
}

int
highest_scenic_score(void)
{
	int score = 0;
	for (size_t i = 0; i < SIZE; ++i) {
		const size_t x = i % LEN;
		const size_t y = i / LEN;
		if (!is_edge(x, y) && is_visible(x, y)) {
			const int d = scenic_score(x, y);
			if (d > score)
				score = d;
		}

	}
	return score;
}

int
main(void)
{
	size_t i = 0;
	for (int c = fgetc(stdin); c != EOF; c = fgetc(stdin)) {
		if (c != '\n')
			trees[i++] = c - '0';
	}

	const int ans1 = visible_trees();
	const int ans2 = highest_scenic_score();

	printf("#1: %d\n", ans1);
	printf("#2: %d\n", ans2);

	return 0;
}
