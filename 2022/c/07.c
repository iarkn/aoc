#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_DIR_SIZE   100000
#define MAX_DISK_SPACE 70000000
#define MIN_FREE_SPACE 30000000

enum file_type {
	FT_FILE, FT_DIR,
};

struct file {
	char name[512];
	enum file_type type;
	size_t size;

	struct file *next;
	struct file *child;
	struct file *parent;
};

struct file_tree {
	struct file *root;
	struct file *curr;
};

struct file *
file_create(enum file_type type, size_t size)
{
	struct file *f = malloc(sizeof(*f));
	*f->name = '\0';
	f->type = type;
	f->size = size;
	f->next = NULL;
	f->child = NULL;
	f->parent = NULL;
	return f;
}

size_t
file_calculate_size(struct file *f)
{
	for (struct file *p = f->child; p != NULL; p = p->next)
		f->size += file_calculate_size(p);
	return f->size;
}

void
dir_add(struct file *dir, struct file *f)
{
	struct file **p = &dir->child;
	while (*p != NULL)
		p = &((*p)->next);
	f->parent = dir;
	*p = f;
}

size_t
parse_cd(struct file_tree *ft, const char *src)
{
	if (src[0] == '/') {
		ft->curr = ft->root;
		return 2;
	} else if (src[0] == '.') {
		ft->curr = ft->curr->parent;
		return 3;
	}
	size_t len = strchr(src, '\n') - src;
	for (struct file *p = ft->curr->child; p != NULL; p = p->next) {
		if (p->type == FT_DIR && strncmp(src, p->name, len) == 0) {
			ft->curr = p;
			return len + 1;
		}
	}
	return 0;
}

size_t
parse_name(char *dst, const char *src)
{
	size_t i;
	for (i = 0; src[i] != '\n'; ++i)
		dst[i] = src[i];
	return i;
}

size_t
parse_ls(struct file_tree *ft, const char *src)
{
	struct file *dir = ft->curr;
	struct file *f;
	size_t i;
	for (i = 0; src[i] != '\0'; ++i) {
		char c = src[i];
		if (isdigit(c)) {
			size_t sz = 0;
			for (c = src[i]; isdigit(c); c = src[++i])
				sz = sz * 10 + (c - '0');
			f = file_create(FT_FILE, sz);
			i += parse_name(f->name, &src[i + 1]);
			dir_add(dir, f);
		} else if (c == 'd') {
			f = file_create(FT_DIR, 0);
			i += parse_name(f->name, &src[i + 4]) + 4;
			dir_add(dir, f);
		} else if (c != '\n') {
			break;
		}
	}
	return i;
}

int
parse(struct file_tree *ft, const char *src)
{
	for (size_t i = 0; src[i] != '\0'; ++i) {
		char c = src[i];
		if (c != '$') {
			fprintf(stderr, "%zu: expected '$', got '%c' (%d)\n",
				i, c, c);
			return 1;
		}
		i += 2;
		c = src[i];
		if (c == 'c') {
			i += parse_cd(ft, &src[i + 3]) + 2;
		} else if (c == 'l') {
			i += parse_ls(ft, &src[i + 3]) + 2;
		} else {
			fprintf(stderr, "cmd: unknown token '%c' (%d)\n", c, c);
			return 1;
		}
	}
	return 0;
}

size_t
find_total_le_100k(const struct file *dir)
{
	size_t total = 0;
	for (struct file *p = dir->child; p != NULL; p = p->next) {
		if (p->type == FT_DIR)
			total += find_total_le_100k(p);
	}
	if (dir->size <= MAX_DIR_SIZE)
		total += dir->size;
	return total;
}

size_t
find_dir_to_delete(const struct file *dir, size_t req)
{
	size_t sz = 0;
	if (dir->size >= req)
		sz = dir->size;
	for (struct file *p = dir->child; p != NULL; p = p->next) {
		if (p->type == FT_DIR) {
			size_t d = find_dir_to_delete(p, req);
			if (d < sz && d != 0)
				sz = d;
		}
	}
	return sz;
}

#if 0
void
print_files_level(const struct file *f, int level)
{
	for (int i = 0; i < level; ++i)
		printf(". ");
	printf("%s: %s (%zu)\n", f->type == FT_DIR ? "d" : "f", f->name, f->size);
	for (struct file *p = f->child; p != NULL; p = p->next)
		print_files_level(p, level + 1);
}

void
print_files(const struct file *f)
{
	print_files_level(f, 0);
}
#endif

int
main(void)
{
	fseek(stdin, 0, SEEK_END);
	const size_t len = ftell(stdin);
	rewind(stdin);

	char *buf = malloc(len + 1);
	const size_t nread = fread(buf, sizeof(*buf), len, stdin);
	if (nread != len) {
		fprintf(stderr, "fread failed: %zu\n", nread);
		goto finish;
	}
	buf[nread] = '\0';

	struct file_tree ft = {0};
	ft.root = file_create(FT_DIR, 0);

	const int err = parse(&ft, buf);
	if (err)
		goto finish;

	const size_t ftsz = file_calculate_size(ft.root);
	const size_t ans1 = find_total_le_100k(ft.root);
	const size_t ans2 = find_dir_to_delete(ft.root,
		MIN_FREE_SPACE - (MAX_DISK_SPACE - ftsz));

	printf("#1: %zu\n", ans1);
	printf("#2: %zu\n", ans2);

finish:
	// Pretend that ft also gets freed here.
	free(buf);
	return err;
}
