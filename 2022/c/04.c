#include <stdio.h>

int
rg_within(int *r1, int *r2)
{
	return r2[0] <= r1[0] && r2[1] >= r1[1];
}

int
rg_overlap(int *r1, int *r2)
{
	int min = r1[0] <= r2[0] ? r1[0] : r2[0];
	int max = r1[1] >= r2[1] ? r1[1] : r2[1];
	for (int i = min; i <= max; ++i) {
		if (r1[0] <= i && i <= r1[1] && r2[0] <= i && i <= r2[1]) {
			return 1;
		}
	}
	return 0;
}

int
parsenum(FILE *f)
{
	int c, res = 0;
	for (c = fgetc(f); '0' <= c && c <= '9'; c = fgetc(f)) {
		res = res * 10 + c - '0';
	}
	ungetc(c, f);
	return res;
}

int
main(void)
{
	int contain = 0;
	int overlap = 0;
	int pair[2][2];
	int p = 0, e = 0;
	for (int c = getchar(); !feof(stdin); c = getchar()) {
		if ('0' <= c && c <= '9') {
			ungetc(c, stdin);
			pair[p][e] = parsenum(stdin);
		} else if (c == '-') {
			e = 1;
		} else if (c == ',') {
			p = 1;
			e = 0;
		} else if (c == '\n') {
			p = e = 0;
			if (rg_within(pair[0], pair[1])
					|| rg_within(pair[1], pair[0])) {
				contain += 1;
			}
			if (rg_overlap(pair[0], pair[1])
					|| rg_overlap(pair[1], pair[0])) {
				overlap += 1;
			}
		} else {
			fprintf(stderr, "unknown token '%c' (%d)\n", c, c);
			return 1;
		}
	}

	printf("#1: %d\n", contain);
	printf("#2: %d\n", overlap);

	return 0;
}
