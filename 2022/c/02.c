#include <stdio.h>

#define WINVAL ((1 << 3) - 1)

enum kind {
    ROCK = 0, PAPER, SCISSORS
};

enum result {
    LOSE = 0, DRAW, WIN
};

const int rps_map[3][3] = {
    {DRAW, LOSE, WIN},
    {WIN, DRAW, LOSE},
    {LOSE, WIN, DRAW}
};

const int rps_mapinv[3][3] = {
    {SCISSORS, ROCK, PAPER},
    {ROCK, PAPER, SCISSORS},
    {PAPER, SCISSORS, ROCK},
};

int main(void)
{
    int total = 0, total2 = 0;
    int a, b;
    for (int c = getchar(); !feof(stdin); c = getchar()) {
        switch (c) {
        case 'A':
        case 'B':
        case 'C':
            b = c - 'A';
            break;
        case 'X':
        case 'Y':
        case 'Z':
            a = c - 'X';
            break;
        case '\n':
            total += rps_map[a][b] * 3 + a + 1;
            total2 += rps_mapinv[b][a] + 1 + a * 3;
            break;
        case ' ':
            break;
        default:
            fprintf(stderr, "invalid token '%c' (%d)\n", c, c);
            return 1;
        }
    }

    printf("#1: %d\n", total);
    printf("#2: %d\n", total2);

    return 0;
}
