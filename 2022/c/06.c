#include <stdio.h>
#include <stdlib.h>

int
chdup(const char *str, char c, size_t n)
{
	for (size_t i = 0; i < n; ++i) {
		if (c == str[i]) {
			return 1;
		}
	}
	return 0;
}

int
marker(const char *src, size_t len, char *buf, size_t n)
{
	for (size_t i = 0; i < len - n; ++i) {
		int dup = 0;
		for (size_t j = 0; j < n; ++j) {
			char c = src[i + j];
			if (chdup(buf, c, j)) {
				dup = 1;
				break;
			}
			buf[j] = c;
		}
		if (!dup) {
			return i + n;
		}
	}
	return -1;
}

int
main(void)
{
	fseek(stdin, 0, SEEK_END);
	size_t nbuf = ftell(stdin);
	rewind(stdin);

	char *buf = malloc(nbuf);
	size_t nread = fread(buf, sizeof(*buf), nbuf, stdin);
	if (nread != nbuf) {
		fprintf(stderr, "fread failed: %zu\n", nread);
		return 1;
	}

	char pack[4];
	char msg[14];
	int spacket = marker(buf, nbuf, pack, 4);
	int smessage = marker(buf, nbuf, msg, 14);

	printf("#1: %d\n", spacket);
	printf("#2: %d\n", smessage);

	free(buf);
	return 0;
}
