#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>

#define CRT_WIDTH 40
#define CRT_HEIGHT 6

struct crt {
	int x, y;
	int cycle;
	int strength;
	bool screen[CRT_WIDTH * CRT_HEIGHT];
};

void
crt_update(struct crt *crt)
{
	crt->cycle += 1;
	if (crt->cycle <= 220 && (crt->cycle == 20 || (crt->cycle - 20) % 40 == 0))
		crt->strength += crt->x * crt->cycle;
	const int x = (crt->cycle - 1) % CRT_WIDTH;
	if (crt->x - 1 <= x && x <= crt->x + 1)
		crt->screen[CRT_WIDTH * crt->y + x] = true;
	if (crt->cycle % 40 == 0)
		crt->y += 1;
}

void
crt_display(const struct crt *crt)
{
	for (size_t i = 0; i < CRT_WIDTH * CRT_HEIGHT; ++i) {
		putchar(crt->screen[i] ? '#' : '.');
		if ((i + 1) % CRT_WIDTH == 0)
			putchar('\n');
	}
}

int
parsenum(FILE *f)
{
	int n = 0;
	int c = fgetc(f);
	const bool negative = c == '-';
	if (negative)
		c = fgetc(f);
	while (isdigit(c)) {
		n = n * 10 + (c - '0');
		c = fgetc(f);
	}
	ungetc(c, f);
	return negative ? -n : n;
}

void
fskip(FILE *f, int n)
{
	while (n--)
		fgetc(f);
}

int
main(void)
{
	struct crt crt = { .x = 1 };
	FILE *f = stdin;
	for (int c = fgetc(f); c != EOF; c = fgetc(f)) {
		if (c == 'a') {
			fskip(f, 4);
			crt_update(&crt);
			crt_update(&crt);
			crt.x += parsenum(f);
		} else if (c == 'n') {
			fskip(f, 3);
			crt_update(&crt);
		}
		fgetc(f);
	}

	printf("#1: %d\n", crt.strength);
	printf("#2:\n");
	crt_display(&crt);
}
