#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int parse_code(int *code)
{
    for (size_t pos = 0;; pos += 4) {
        int a, b, dst;
        switch (code[pos]) {
        case 1: // add
            a = code[pos + 1], b = code[pos + 2];
            dst = code[pos + 3];
            code[dst] = code[a] + code[b];
            break;
        case 2: // mul
            a = code[pos + 1], b = code[pos + 2];
            dst = code[pos + 3];
            code[dst] = code[a] * code[b];
            break;
        case 99: // end
            return 0;
        default:
            fprintf(stderr, "unknown opcode: %d\n", code[pos]);
            return -1;
        }
    }
}

int main(void)
{
    char *buf = NULL;
    size_t n = 0;
    ssize_t nread;

    int data[181];
    int tmp[181];
    size_t len;
    for (len = 0; (nread = getdelim(&buf, &n, ',', stdin)) != -1; ++len) {
        int d = strtol(buf, NULL, 10);
        data[len] = tmp[len] = d;
    }
    free(buf);
    if (nread == -1 && !feof(stdin)) {
        fprintf(stderr, "failed to read input: %s\n", strerror(errno));
        return 1;
    }

    data[1] = tmp[1] = 12;
    data[2] = tmp[2] = 2;
    if (parse_code(tmp) < 0) return 1;
    printf("#1: %d\n", tmp[0]);

    const int want = 19690720;
    for (size_t i = 0; i < 99; ++i) {
        for (size_t j = 0; j < 99; ++j) {
            memmove(tmp, data, len * sizeof(*data));
            tmp[1] = i;
            tmp[2] = j;
            if (parse_code(tmp) < 0) return 1;
            if (tmp[0] == want) {
                printf("#2: %zu\n", 100 * i + j);
                return 0;
            }
        }
    }

    fputs("unable to solve #2\n", stderr);
    return 1;
}
