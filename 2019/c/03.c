#include <stdio.h>
#include <stdlib.h>

struct pos {
    int x, y;
};

int parse_num(FILE *f)
{
    int c;
    int res = 0;
    for (c = fgetc(f); '0' <= c && c <= '9'; c = fgetc(f)) {
        res = res * 10 + c - '0';
    }
    ungetc(c, f);
    return res;
}

struct pos get_dir(int c)
{
    struct pos dir = {0, 0};
    switch (c) {
    case 'R': dir.x = 1;  break;
    case 'L': dir.x = -1; break;
    case 'U': dir.y = 1;  break;
    case 'D': dir.y = -1; break;
    default: break;
    }
    return dir;
}

int main(void)
{
    size_t cap = 32;
    size_t len = 0;
    struct pos *wire = malloc(cap * sizeof(*wire));
    struct pos p = {0, 0};

    for (int c = getchar(); c != '\n'; c = getchar()) {
        if (c == ',') continue;
        struct pos dir = get_dir(c);
        int n = parse_num(stdin);

        p.x += n * dir.x;
        p.y += n * dir.y;
        wire[len++] = p;
        if (cap < len) {
            cap *= 2;
            wire = realloc(wire, cap * sizeof(*wire));
        }
    }

    p.x = 0;
    p.y = 0;
    int closest = -1;
    for (int c = getchar(); c != '\n'; c = getchar()) {
        if (c == ',') continue;
        struct pos dir = get_dir(c);
        int n = parse_num(stdin);

        for (int i = 0; i < n; ++i) {
            p.x += dir.x;
            p.y += dir.y;

            for (size_t j = 0; j < len - 1; ++j) {
                struct pos from = wire[j];
                struct pos to = wire[j + 1];

                int minx = from.x < to.x ? from.x : to.x;
                int maxx = from.x < to.x ? to.x : from.x;
                int miny = from.y < to.y ? from.y : to.y;
                int maxy = from.y < to.y ? to.y : from.y;

                int within_x = p.x == from.x && p.x == to.x && miny <= p.y
                    && p.y <= maxy;
                int within_y = p.y == from.y && p.y == to.y && minx <= p.x
                    && p.x <= maxx;

                if (within_x || within_y) {
                    int dst = abs(p.x) + abs(p.y);
                    if (closest > dst || closest == -1) {
                        closest = dst;
                    }
                }
            }
        }
    }

    printf("#1: %d\n", closest);

    free(wire);
    return 0;
}
