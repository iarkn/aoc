#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    char *buf = NULL;
    size_t n = 0;
    ssize_t nread;

    int fuelsum = 0;
    int fuelsumrec = 0;
    while ((nread = getline(&buf, &n, stdin)) != -1) {
        int mass = strtol(buf, NULL, 10);
        int fuel = mass / 3 - 2;
        fuelsum += fuel;

        while (fuel > 0) {
            fuelsumrec += fuel;
            fuel = fuel / 3 - 2;
        }
    }
    free(buf);
    if (nread == -1 && !feof(stdin)) {
        fprintf(stderr, "failed to read input: %s\n", strerror(errno));
        return 1;
    }

    printf("#1: %d\n", fuelsum);
    printf("#2: %d\n", fuelsumrec);

    return 0;
}
